<?PHP
include ( 'common.php' ) ;

$language = get_request ( 'language' , 'de' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;

$mysql_con = db_get_con_new($language,$project) ;
$db = get_db_name ( $language , $project ) ;

print "<html><head></head><body>" ;
print get_common_header ( "creationist.php" , "Creationist" ) ;

print "<p>Running query, standby...</p>" ; myflush() ;


$sql = "SELECT log_title,log_timestamp from logging where log_namespace=0 AND log_type='delete' and log_timestamp>'20091011214333' AND log_title IN (SELECT rc_title FROM recentchanges WHERE rc_namespace=0 AND rc_new=1 ) " ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
$pages = array () ;
while ( $o = mysql_fetch_object ( $res ) ) {
	$pages[$o->log_title] = $o ;
}

$sql = "SELECT rc_title,rc_timestamp FROM recentchanges WHERE rc_namespace=0 AND rc_new=1" ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
while ( $o = mysql_fetch_object ( $res ) ) {
	if ( !isset ( $pages[$o->rc_title] ) ) continue ;
	$pages[$o->rc_title]->rc_timestamp = $o->rc_timestamp ;
	if ( $pages[$o->rc_title]->rc_timestamp > $pages[$o->rc_title]->log_timestamp ) unset ( $pages[$o->rc_title] ) ;
	else print "<li>" . $o->rc_title . " : " . $pages[$o->rc_title]->rc_timestamp . " - " . $pages[$o->rc_title]->log_timestamp . "</li>" ;
}



$sum = 0 ;
$cnt = 0 ;
$med = array () ;
$tp = array () ;

foreach ( $pages AS $page => $o ) {
	$diff = strtotime ( $o->log_timestamp ) - strtotime ( $o->rc_timestamp ) ;
	$tp[(int)$diff/3600/24]++ ;
	$sum += $diff ;
	$cnt++ ;
	$med[] = $diff ;
}

ksort ( $tp ) ;
asort ( $med ) ;

print "<p>The following data applies to all articles that were created and deleted within the timeframe of Recent Changes (about 1 month)</p>" ;
print "<ul>" ;
print "<li>Articles : $cnt</li>" ;
print "<li>Average lifetime: " . round ( $sum / $cnt / 3600 )  . " hours</li>" ;
print "<li>Median lifetime: " . $med[(int)(count($med)/2)]  . " hours</li>" ;

foreach ( $tp AS $k => $v ) {
	print "<li>$k hours : $v &times;</li>" ;
}

print "</ul>" ;
print "</body></html>" ;

?>