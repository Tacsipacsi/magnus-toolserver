﻿<?PHP
error_reporting ( E_ALL ) ;
$suppress_gz_handler = 1 ;
@set_time_limit ( 15*60 ) ; # Time limit

include_once ( 'queryclass.php' ) ;
high_mem ( 256 ) ;

function get_image_pages ( $ns , &$image_pages ) {
	global $language , $project ;
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;

	$image_pages = array () ;
	$sql = "SELECT ".get_tool_name()." /* SLOW_OK */ page_title FROM page WHERE page_namespace=$ns" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) {
		print mysql_error() ;
		exit ;
	}
	while ( $o = mysql_fetch_object ( $res ) ) {
		$image_pages[$o->page_title] = 1 ;
	}
}

function remove_images_from_list ( $language , $project , &$ip ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;

	$sql = "SELECT ".get_tool_name()." /* SLOW_OK */ img_name FROM image" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) {
		print mysql_error() ;
		exit ;
	}
	while ( $o = mysql_fetch_object ( $res ) ) {
		if ( isset ( $ip[$o->img_name] ) ) unset ( $ip[$o->img_name] ) ;
	}
}

function remove_images_from_list2 ( $language , $project , &$ip_orig ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;

	$ip2 = array_keys ( $ip_orig ) ;
	while ( count ( $ip2 ) > 0 ) {
		$ip = array () ;
		while ( count ( $ip ) < 200 and count ( $ip2 ) > 0 ) {
			$ip[] = array_pop ( $ip2 ) ;
		}
		foreach ( $ip AS $k => $v ) {
			make_db_safe ( $v ) ;
			$ip[$k] = '"' . $v . '"' ;
		}
		$ip = implode ( ',' , $ip ) ;

		$sql = "SELECT ".get_tool_name()." img_name FROM image WHERE img_name IN ( $ip ) " ;
		$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) {
			print mysql_error() ;
			exit ;
		}
		while ( $o = mysql_fetch_object ( $res ) ) {
			if ( isset ( $ip_orig[$o->img_name] ) ) unset ( $ip_orig[$o->img_name] ) ;
		}
	}
}

function remove_pages_with_allowed_templates ( $ns , &$pages , $allowed_templates ) {
	global $language , $project , $nsdata , $testing ;
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;
	
	$allowed = array () ;
	foreach ( $allowed_templates AS $v ) $allowed[$v] = 1 ;

	$ip2 = array_keys ( $pages ) ;
	while ( count ( $ip2 ) > 0 ) {
		$ip = array () ;
		while ( count ( $ip ) < 200 and count ( $ip2 ) > 0 ) {
			$ip[] = array_pop ( $ip2 ) ;
		}
		foreach ( $ip AS $k => $v ) {
			make_db_safe ( $v ) ;
			$ip[$k] = '"' . $v . '"' ;
		}
		$ip = implode ( ',' , $ip ) ;

		$sql = "SELECT ".get_tool_name()." page_title,tl_namespace,tl_title FROM page,templatelinks WHERE page_namespace=$ns AND page_id=tl_from AND page_title IN ( $ip ) " ;
		
		
		
		$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) {
			print mysql_error() ;
			exit ;
		}
		while ( $o = mysql_fetch_object ( $res ) ) {
			$tn = $nsdata[$o->tl_namespace] . $o->tl_title ;
			if ( isset ( $allowed[$tn] ) and isset ( $pages[$o->page_title] ) ) unset ( $pages[$o->page_title] ) ;
			if ( $o->tl_namespace != 10 ) continue ;
			$tn = $o->tl_title ;
			if ( isset ( $allowed[$tn] ) and isset ( $pages[$o->page_title] ) ) unset ( $pages[$o->page_title] ) ;
		}
	}
}

function no_desc_on_commons ( &$talk ) {
	$mysql_con = db_get_con_new('commons','wikimedia') ;
	$db = get_db_name ( 'commons','wikimedia' ) ;
	
	$ip2 = array_keys ( $talk ) ;
	while ( count ( $ip2 ) > 0 ) {
		$ip = array () ;
		while ( count ( $ip ) < 200 and count ( $ip2 ) > 0 ) {
			$ip[] = array_pop ( $ip2 ) ;
		}
		foreach ( $ip AS $k => $v ) {
			make_db_safe ( $v ) ;
			$ip[$k] = '"' . $v . '"' ;
		}
		$ip = implode ( ',' , $ip ) ;

		$sql = "SELECT ".get_tool_name()." page_title FROM page WHERE page_namespace=6 AND page_title IN ( $ip ) " ;
		$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) {
			print mysql_error() ;
			exit ;
		}
		while ( $o = mysql_fetch_object ( $res ) ) {
			unset ( $talk[$o->page_title] ) ;
		}
	}
}





$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$mode = get_request ( "mode" , "123" ) ;
$testing = isset ( $_REQUEST['testing'] ) ;


print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "image_pages_without_image.php" ) . "\n" ;
myflush() ;


$allow_templates_page = "Benutzer:Forrester/lizenzvorlagen.js" ;
$text = file_get_contents ( "http://$language.$project.org/w/index.php?action=raw&title=$allow_templates_page" ) ;
$lines = explode ( "\n" , $text ) ;
$recording = false ;
foreach ( $lines AS $l ) {
	if ( $recording && preg_match ( '/<\/ruleset>/' , $l ) ) break ;
	
	if ( preg_match ( '/<ruleset\stype="no\scommons".*>/i' , $l ) ) {
		$recording = true ;
		continue ;
	}
	
	if ( !$recording ) continue ;
	$a = array () ;
	preg_match ( '/\stl_title="(.+)"/' , $l , $a ) ;
	$l = str_replace ( ' ' , '_' , $a[1] ) ;
	$allowed_templates[] = $l ;
}


/*
$text = array_pop ( explode ( 'type="allow"' , $text , 2 ) ) ;
$text = array_shift ( explode ( '</RuleSet>' , $text , 2 ) ) ;
$matches = array () ;
preg_match_all ( '/title="([^"]+)"/' , $text , $matches ) ;
$allowed_templates = array () ;
foreach ( $matches[1] AS $v ) {
	$allowed_templates[] = str_replace ( ' ' , '_' , $v ) ;
}
*/






$url = "http://$language.$project.org/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=php" ;
$nsdata = unserialize ( file_get_contents ( $url ) ) ;
$nsdata = $nsdata['query']['namespaces'] ;
foreach ( $nsdata AS $k => $v ) {
	$v = $v['*'] ;
	if ( $v != '' ) $v .= ':' ;
	$nsdata[$k] = $v ;
}





print "<p>Getting all image description pages on $language.$project...</p>\n" ; myflush() ;
$image_pages = array () ;
get_image_pages ( 6 , $image_pages ) ;
print "<p>" . count ( $image_pages ) . " image description pages found.</p>" ;
myflush() ;

print "<p>Getting all image <i>talk</i> pages on $language.$project...</p>\n" ; myflush() ;
$image_talk_pages = array () ;
get_image_pages ( 7 , $image_talk_pages ) ;
print "<p>" . count ( $image_talk_pages ) . " image talk pages found.</p>" ;
myflush() ;

foreach ( $image_pages AS $k => $v ) {
	if ( isset ( $image_talk_pages[$k] ) ) unset ( $image_talk_pages[$k] ) ;
}
no_desc_on_commons ( $image_talk_pages ) ;


print "Removing image description pages with local images...</p>\n" ; myflush() ;
remove_images_from_list ( $language , $project , $image_pages ) ;
print "<p>" . count ( $image_pages ) . " image description pages without local image.</p>" ;
myflush() ;



$ip2 = $image_pages ;
print "Removing those with commons images...</p>\n" ; myflush() ;
remove_images_from_list2 ( 'commons' , 'wikimedia' , $image_pages ) ;
print "<p>" . count ( $image_pages ) . " image description pages without any image.</p>" ;




if ( false !== strpos ( $mode , '1' ) ) {
	print "<h3>Image description pages on $language.$project without any image</h3>\n" ;
	print "<ol>" ;
	foreach ( $image_pages AS $k => $v ) {
		print "<li><a target='_blank' href=\"http://$language.$project.org/wiki/Image:$k\">$k</a></li>" ;
	}
	print "</ol>\n" ;
	myflush() ;
}


/*
if ( $language == 'de' && $project == 'wikipedia' ) {
	$wq = new WikiQuery ( $language , $project ) ;
	$badcats = array ( 'Wikipedia:Dateiüberprüfung/1923' ) ;
	$bad_removed = 0 ;
	foreach ( $image_talk_pages AS $k => $v ) {
		$data = $wq->get_categories ( "File talk:$k" ) ;
		$isbad = false ;
		foreach ( $badcats AS $c ) {
			if ( !in_array ( $c , $data ) ) continue ;
			$isbad = true ;
			break ;
		}
		if ( $isbad ) {
			unset ( $image_talk_pages[$k] ) ;
			$bad_removed++ ;
		}

	}
	if ( $bad_removed > 0 ) {
		print "<p>$bad_removed talk pages ignored because of categories.</p>" ;
	}
}
*/


if ( false !== strpos ( $mode , '2' ) ) {
	print "<h3>Image talk pages without image description page (" . count ( $image_talk_pages ) . ")</h3>" ;
	print "<ol>" ;
	foreach ( $image_talk_pages AS $k => $v ) {
		print "<li><a target='_blank' href=\"http://$language.$project.org/wiki/Image_talk:$k\">$k</a></li>" ;
	}
	print "</ol>" ;
	myflush() ;
}



print "Removing image description pages with \"allowed\" templates...</p>\n" ; myflush() ;
remove_pages_with_allowed_templates ( 6 , $ip2 , $allowed_templates ) ;
print "<p>Still " . count ( $ip2 ) . " image description pages without local image.</p>" ;
myflush() ;


foreach ( $image_pages AS $k => $v ) {
	unset ( $ip2[$k] ) ;
}


if ( false !== strpos ( $mode , '3' ) ) {
	print "<h3>Image description pages on $language.$project without a local image (" . count ( $ip2 ) . ")</h3>\n" ;
	print "<ol>" ;
	foreach ( $ip2 AS $k => $v ) {
		print "<li><a target='_blank' href=\"http://$language.$project.org/wiki/Image:$k\">$k</a></li>" ;
	}
	print "</ol>\n" ;
	myflush() ;
}


/*
if ( count ( $image_pages ) > 0 ) {
	print "<h3>Image description pages on $language.$project without a local image</h3>\n" ;
	print "<ol>" ;
	foreach ( $image_pages AS $k => $v ) {
		print "<li><a target='_blank' href=\"http://$language.$project.org/wiki/Image:$k\">$k</a></li>" ;
	}
	print "</ol>\n" ;
	myflush() ;
}
*/

print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
