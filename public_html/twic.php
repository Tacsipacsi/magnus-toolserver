<?PHP


include_once ( "common.php" ) ;
include_once ( 'transwiki_data.php' ) ;

function twic_get_xml ( $image , $language , $project ) {
	$url = "http://toolserver.org/~magnus/wiki2xml/w2x.php?whatsthis=articlelist&use_templates=none&site=$language.$project.org/w&text=Image:$image&output_format=xml&add_gfdl=0&doit=1" ;
#	print "$url<br/>" ;
	return file_get_contents ( $url ) ;
}

$twic_tags = array () ;
$twic_attrs = array () ;
$twic_last_arg_name = '' ;

$twic = '' ;
$twic->last_arg_name = '' ;
$twic->last_template_name = '' ;
$twic->info_template_name = 'Information' ;
$twic->infoinfo = array () ;
$twic->in_heading = false ;

function startElement($parser, $tag, $attrs) {
	global $twic_tags , $twic_attrs , $twic ;
	if ( $tag == 'SPACE' ) return ;
	if ( $tag == 'ARG' && $twic_tags[count($twic_tags)-1] == 'TEMPLATE' ) {
		$twic->last_arg_name = $attrs['NAME'] ;
	}
	array_push ( $twic_tags , $tag ) ;
	array_push ( $twic_attrs , $attrs ) ;
	if ( $tag == 'HEADING' ) $twic->in_heading = true ;
	print "BEGIN : $tag : " . implode ( ',' , array_keys ( $attrs ) ) . " | " . implode ( ',' , $attrs ) . "<br/>" ;
}

function endElement($parser, $tag) {
	global $twic_tags , $twic_attrs , $twic ;
	if ( $tag == 'TEMPLATE' ) $twic->last_template_name = '' ;
	if ( $tag == 'SPACE' ) {
		characterData ( $parser , ' ' ) ;
		return ;
	}
	array_pop ( $twic_tags ) ;
	array_pop ( $twic_attrs ) ;
	if ( $tag == 'HEADING' ) $twic->in_heading = false ;
	print "END : $tag<br/>" ;
}

function characterData($parser, $data) {
	global $twic_tags , $twic_attrs , $twic ;
	$tag = $twic_tags[count($twic_tags)-1] ;
	if ( $twic->in_heading ) return ;
	if ( $tag == 'TARGET' and $twic_tags[count($twic_tags)-2] == 'TEMPLATE' ) {
		$twic->last_template_name = ucfirst ( str_replace ( '_' , ' ' , $data ) ) ;
		$twic->last_arg_name = '' ;
#		return ;
	}
	if ( $twic->info_template_name == $twic->last_template_name ) {
		if ( $twic->last_arg_name != '' ) $twic->info[$twic->last_arg_name] .= $data ;
		return ;
	}
	print "DATA : $data <i>{$twic->last_template_name}/{$twic->last_arg_name}</i><br/>" ;
}

function twic_xml_parse ( $s ) {
	$xml_parser = xml_parser_create();
	// use case-folding so we are sure to find the tag in $map_array
	xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
	xml_set_element_handler($xml_parser, "startElement", "endElement");
	xml_set_character_data_handler($xml_parser, "characterData");
	
	if (!xml_parse($xml_parser, $s, false)) {
		die(sprintf("XML error: %s at line %d",
					xml_error_string(xml_get_error_code($xml_parser)),
					xml_get_current_line_number($xml_parser)));
	}

	xml_parser_free($xml_parser);
}




$source_image = '1852Hippo.jpg' ;
$source_language = 'de' ;
$source_project = 'wikipedia' ;

if ( isset ( $info_template_names[$source_language] ) ) {
	$twic->info_template_name = $info_template_names[$source_language] ;
}

print "<html><head><META charset=\"UTF-8 http-equiv=Content-Type content=text/html; charset=utf-8\"></head><body>" ;
twic_xml_parse ( twic_get_xml ( $source_image , $source_language , $source_project) ) ;

foreach ( $twic->info AS $k => $v ) {
	$v = trim ( $v ) ;
	if ( isset ( $information_keys[$k] ) ) $k = ucfirst ( $information_keys[$k] ) ;
	$v = preg_replace ( '/\[\[([^|\]]+)\]\]/' , '[[${1}|${1}]]' , $v ) ;
	$v = str_replace ( '[[' , "[[:$source_language:" , $v ) ;
	if ( $k == 'Beschreibung' ) $info['Description'] = '{{' . $source_language . "|$v}}"  ;
	if ( $k == 'Quelle' ) $info['Source'] = $v ;
	if ( $k == 'Urheber' ) $info['Author'] = $v ;
	if ( $k == 'Datum' ) $info['Date'] = $v ;
	if ( $k == 'Genehmigung' ) $info['Permission'] = $v ;
	if ( $k == 'Andere Versionen' ) $info['Other versions'] = $v ;
#	if ( $k == 'Anmerkungen' ) $info['Comments'] = $v ;
}

if ( count ( $info ) > 0 ) {
	print "<pre>" ;
	print "{{Information\n" ;
	foreach ( $info AS $k => $v ) {
		print "|$k=$v\n" ;
	}
	print "}}\n" ;
	print "</pre>" ;
}

print "</body></html>" ;

?>