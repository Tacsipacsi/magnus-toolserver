<?PHP

$t1 = microtime ( true ) ;

include "common.php" ;
high_mem ( 100 ) ;

$trcnt = 1 ;

function check_double ( $basename ) {
	global $data , $trcnt ;
	if ( count ( $data[$basename] ) != 2 ) return ;
	
	$cj = explode ( '|' , $data[$basename]['jpg'] ) ;
	$ct = explode ( '|' , $data[$basename]['tif'] ) ;
	
	$joined = array() ;
	foreach ( $cj AS $c ) $joined[$c]++ ;
	foreach ( $ct AS $c ) $joined[$c]++ ;
	foreach ( array ( 'High-resolution TIFF images from the National Archives and Records Administration' ) AS $c ) {
		unset ( $joined[str_replace(' ','_',$c)] ) ;
	}

	$not2 = 0 ;
	foreach ( $joined AS $k => $v ) {
		if ( $v == 2 ) continue ;
		$cont = false ;
		foreach ( array ( 'Featured picture' , 'Media from the National Archives and Records Administration needing categories' ) AS $k2 => $cs ) {
			if ( substr ( $k , 0 , strlen ( $cs ) ) == str_replace ( ' ' , '_' , $cs ) ) $cont = true ;
		}
		if ( $cont ) {
			$joines[$k] = 10 ; // Tag for later
			continue ;
		}
		$not2++ ;
	}
	if ( $not2 == 0 ) {
		unset ( $data[$basename] ) ;
		return ;
	}
	
	$s = '' ;
	$s .= "<tr>" ;
	$s .= "<th>$trcnt</th>" ;
	$s .= "<td>$basename<br/>" ;
	$s .= "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" . myurlencode($basename) . ".jpg'>JPEG</a> | " ;
	$s .= "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" . myurlencode($basename) . ".tif'>TIFF</a>" ;
	$s .= "</td>" ;
	
	$s .= "<td>" ;
	foreach ( $cj AS $c ) {
		if ( !isset($joined[$c]) or $joined[$c] > 2 ) $s .= "<div style='color:#BBBBBB'>$c</div>" ;
		else if ( $joined[$c] == 1 ) $s .= "<div style='color:red'>$c</div>" ;
		else $s .= "<div>$c</div>" ;
	}
	$s .= "</td>" ;

	$s .= "<td>" ;
	foreach ( $ct AS $c ) {
		if ( !isset($joined[$c]) ) $s .= "<div style='color:#BBBBBB'>$c</div>" ;
		else if ( $joined[$c] == 1 ) $s .= "<div style='color:red'>$c</div>" ;
		else $s .= "<div>$c</div>" ;
	}
	$s .= "</td>" ;
	
//	$s .= "<td>" . implode ( "<br/>" , $cj ) . "</td>" ;
//	$s .= "<td>" . implode ( "<br/>" , $ct ) . "</td>" ;
	$s .= "</tr>" ;
	$s = str_replace ( '_' , ' ' , $s ) ;
	
	print $s ;
	
	$trcnt++ ;
	
	unset ( $data[$basename] ) ;
}

print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "filterdone.php" ) ;

print "<div>Jump to : <a href='#no_corresponding'>Files without corresponding other type</a></div>" ;

//print "Getting category tree...<br/>" ; myflush() ;

$language = 'commons' ;
$project = 'wikimedia' ;

$a = array() ;
$basecat = "Images_from_the_National_Archives_and_Records_Administration" ;
$cats = db_get_articles_in_category ( $language , $basecat , 10 , 14 , $a , true , '' , $project ) ;

$cat = array() ;
foreach ( $cats as $c ) $cat[] = mysql_real_escape_string ( $c ) ;

$cat = '"' . implode ( '","' , $cat ) . '"' ;
$sql = "select page_title,group_concat(DISTINCT c2.cl_to SEPARATOR '|') as cats from page,categorylinks c1,categorylinks c2 where page_namespace=6 and c1.cl_from=page_id and c1.cl_from=c2.cl_from and c1.cl_to IN ($cat) group by c1.cl_from" ;
$mysql_con = db_get_con_new($language,$project) ;
$db = get_db_name ( $language , $project ) ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
print "<table border='1' cellspacing=0 cellpadding=2><tr><th>#</th><th>File base name</th><th>JPEG</th><th>TIFF</th></tr>" ;
while ( $o = mysql_fetch_object ( $res ) ) {
	$m = array() ;
	preg_match ( '/^(.+)\.([^.]+)$/i' , $o->page_title , $m ) ;
	$data[$m[1]][$m[2]] = $o->cats ;
	check_double ( $m[1] ) ;
}
print "</table>" ;

print "<hr/><h2><a name='no_corresponding'>Files without corresponding other type</a></h2>" ;
print "<ol>" ;
foreach ( $data AS $k => $v ) {
	$type = '' ;
	foreach ( $v AS $a => $b ) $type = $a ;
	print "<li>" ;
	print "<a target='_blank' href='//commons.wikimedia.org/wiki/File:" . myurlencode ( $k ) . ".$type'>$k.$type</a>" ;
	print "</li>" ;
}
print "</ol>" ;

$t2 = microtime ( true ) ;
$td = sprintf ( "%2.2f seconds" , ($t2-$t1) ) ;

print "<hr/>DONE in $td!" ;
print "</body></html>" ;

?>