<?PHP

error_reporting ( E_ALL ) ;
@set_time_limit ( 60*60 ) ; # Time limit 60 min

include_once ( 'php/wikiquery.php' ) ;
//require ( 'phpFlickr/phpFlickr.php' ) ;
require ( 'phpflickr-3.1/phpFlickr.php' ) ;
require_once( 'peachy/Init.php' );

@set_time_limit ( 60*60 ) ; # Time limit 60 min

function do_direct_upload ( $url , $new_name , $desc ) {
	global $perl_command , $project ;
	$cwd = getcwd() ;
	
	do {
		$temp_name = tempnam ( "/tmp" , "f2c_" ) ;
		$temp = @fopen ( $temp_name , "w" ) ;
	} while ( $temp === false ) ;
	$temp_dir = $temp_name . "-dir" ;
	mkdir ( $temp_dir ) ;
	
	$short_file = str_replace ( " " , "_" , $new_name ) ;
	$short_file = str_replace ( ":" , "_" , $short_file ) ;
	$short_file = str_replace ( "/" , "_" , $short_file ) ;
	$short_file = str_replace ( "\\" , "_" , $short_file ) ;
	$short_file = str_replace ( "'" , "" , $short_file ) ;
	
	$newfile = $temp_dir . "/" . $short_file ;
	if ( !copy($url, $newfile) ) {
		rmdir ( $temp_dir ) ;
		unlink ( $temp_name ) ;
		return "Error" ;
	}
	
  $desc = trim ( $desc ) ;
#  $desc .= "\n{{flickrreview}}" ;
//	$desc = utf8_decode ( $desc ) ;

	$newname = $short_file ;

	$bot_pass = trim ( file_get_contents ( "../upload_bot_key.txt" ) ) ;
	$peach = Peachy::newWiki( null , "File Upload Bot (Magnus Manske)" , $bot_pass , 'http://commons.wikimedia.org/w/api.php' );
	$image = $peach->initImage( $newname );
	$b = $image->upload ( $newfile , $desc ) ;
	$output = $b ; // FIXME

/*
	// Create meta file
	$meta_file = $temp_dir . '/meta.txt' ;
	$meta = @fopen ( $meta_file , "w" ) ;
	fwrite ( $meta , $newfile . "\n" ) ;
	fwrite ( $meta , $newname . "\n" ) ;
	fwrite ( $meta , $desc ) ;
	fclose ( $meta ) ;

	// Run upload bot
	$upload_bot = "./upload_bot.pl" ;
	$command = "{$perl_command} {$upload_bot} {$temp_dir}" ;
	
//	print "DEBUG : $command<br/>" ;
//	$command .= ' 2>&1 1> /dev/null' ;
	
	$output = shell_exec ( $command ) ;
*/

	// Cleanup
	$debug_file = $temp_dir . "/debug.txt" ;
	@unlink ( $debug_file ) ;
	unlink ( $meta_file ) ;
	unlink ( $local_file ) ;
	unlink ( $newfile ) ;
	rmdir ( $temp_dir ) ;
	fclose ( $temp ) ;
	unlink ( $temp_name ) ;

	// Output
	$ret = '' ;//"<h3>Output of upload bot</h3><pre>{$output}</pre>" ;
	$ret .= "The image should now be at <a target='blank' href='" ;
	$ret .= get_wikipedia_url ( 'commons' , 'File:'.$newname ) . "'>{$newname}</a>. " ;
	$ret .= "<a href=\"http://commons.wikimedia.org/w/index.php?action=edit&title=Image:$newname\" target=\"_blank\">Edit the new description page</a>." ;
	return $ret ;
}

function check_words ( $words , $t ) {
	$cont = 1 ;
	foreach ( $words['all'] AS $w ) {
		if ( $w == '' ) continue ;
		if ( FALSE !== stristr ( $t , $w ) ) continue ;
		$cont = 0 ;
		break ;
	}
	if ( $cont == 0 ) return "Skipping - required word \"$w\" not found" ;
	
	$cont = 0 ;
	$req = 0 ;
	foreach ( $words['any'] AS $w ) {
		if ( $w == '' ) continue ;
		$req = 1 ;
		if ( FALSE === stristr ( $t , $w ) ) continue ;
		$cont = 1 ;
		break ;
	}
	if ( $cont == 0 and $req == 1 ) return "Skipping - none of the \"any\" words found" ;
	
	$cont = 1 ;
	foreach ( $words['none'] AS $w ) {
		if ( $w == '' ) continue ;
		if ( FALSE === stristr ( $t , $w ) ) continue ;
		$cont = 0 ;
		break ;
	}
	if ( $cont == 0 ) return "Skipping - prohibitive word \"$w\" found" ;
	
	return '' ; // OK
}



$flickr_stream_url = get_request ( 'flickr_stream_url' , '' ) ;
$flickr_user_name = get_request ( 'flickr_user_name' , '' ) ;
$desc_add = get_request ( 'desc_add' , '' ) ;
$info_add = get_request ( 'info_add' , '' ) ;
$show_only_uploaded = get_request ( 'show_only_uploaded' , '' ) ;
$simulation = get_request ( 'simulation' , '' ) ;
$max_transfer = get_request ( 'max_transfer' , '' ) ;
$test = get_request ( 'test' , '' ) ;
$tusc_user = get_request ( 'tusc_user' , '' ) ;
$tusc_password = get_request ( 'tusc_password' , '' ) ;

$cb_show_only_uploaded = $show_only_uploaded == '' ? '' : 'checked' ;
$cb_simulation = $simulation == '' ? '' : 'checked' ;

$words = array () ;
foreach ( array ( 'either' , 'title' , 'text' , 'tags' ) AS $a ) {
	foreach ( array ( 'all' , 'any' , 'none' ) AS $b ) {
		$words[$a][$b] = get_request ( 'words_'.$a.'_'.$b , '' ) ;
	}
}

print "<html><head></head><body>" ;
print get_common_header ( "flickr2commons.php" , "Flickr2Commons" ) ;
print "<h1>Flickr Mass</h1>" ;
print "This tool can mass-upload suitable flickr images to Wikimedia Commons.<br/>" ;

if ( $test == 1 ) print "TESTING<br/>" ;

print "
<div><b><big>Also, try the <a href='http://toolserver.org/~magnus/ts2/flickr2commons'>new version</a>!</big></b></div><br/>
<form method='post' action='http://toolserver.org/~magnus/flickr_mass.php'>
<table>
<tr><th>Stream URL</th><td><input type='text' size='100' name='flickr_stream_url' value='{$flickr_stream_url}'/> <small><i>e.g. http://www.flickr.com/photos/49327984@N08/</i></small>, <i><b>or</b></i></td></tr>
<tr><th>Flickr user ID</th><td><input type='text' size='100' name='flickr_user_name' value='{$flickr_user_name}'/>
 (<a href='http://idgettr.com/'>get user ID from the user name</a>)
</td></tr>
<tr><th>Add to description</th><td><textarea rows='5' cols='80' name='desc_add'>$desc_add</textarea></tr>
<tr><th>Add after {{Information}}<br><small>(e.g. categories, templates…)</small></th><td><textarea rows='5' cols='80' name='info_add'>$info_add</textarea></td></tr>

<tr><th>Words</th><td>
<table>
<tr><th/><th>Anywhere</th><th>Title</th><th>Description text</th><th>Tags</th></tr>
<tr><th>All</th>
<td><textarea rows='2' cols='25' name='words_either_all'>{$words['either']['all']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_title_all'>{$words['title']['all']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_text_all'>{$words['text']['all']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_tags_all'>{$words['tags']['all']}</textarea></td>
</tr>
<tr><th>Any</th>
<td><textarea rows='2' cols='25' name='words_either_any'>{$words['either']['any']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_title_any'>{$words['title']['any']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_text_any'>{$words['text']['any']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_tags_any'>{$words['tags']['any']}</textarea></td>
</tr>
<tr><th>None</th>
<td><textarea rows='2' cols='25' name='words_either_none'>{$words['either']['none']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_title_none'>{$words['title']['none']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_text_none'>{$words['text']['none']}</textarea></td>
<td><textarea rows='2' cols='25' name='words_tags_none'>{$words['tags']['none']}</textarea></td>
</tr>
</table>
<small>Separate multiple keywords with spaces</small>
</td></tr>

<tr><th rowspan='3'>Options</th><td><input type='checkbox' id='simulation' name='simulation' value='1' {$cb_simulation} /><label for='simulation'>Simulate only (no actual transfer to Commons)</label></td></td>
<tr><td><input type='checkbox' id='show_only_uploaded' name='show_only_uploaded' value='1' {$cb_show_only_uploaded} /><label for='show_only_uploaded'>Show only images that are actually transferred to Commons</label></td></td>
<tr><td>Transfer no more than <input type='number' name='max_transfer' value='{$max_transfer}' /> images (leave blank for unlimited)</td></tr>
<tr><th><a href='./tusc.php'>TUSC</a> login</th><td><input type='text' name='tusc_user' value='$tusc_user' /> (your user name at Wikimedia Commons)</td></tr>
<tr><th>TUSC password</th><td><input type='password' name='tusc_password' value='$tusc_password' /> (<i>NOT</i> your Commons password!)</td></tr>
<tr><th></th><td><input type='submit' name='doit' value='Upload'/> (all fields need to be filled in)</td></tr>
</table>
<input type='hidden' name='test' value='{$test}' />
</form>" ;

if ( !isset ( $_REQUEST['doit'] ) ) {
	print "</body></html>" ;
	exit ( 0 ) ;
}

if ( $test != 1 && ( $tusc_user == '' || $tusc_password == '' ) ) {
	print "TUSC login needs to be provided!<br/>" ;
	exit ( 0 ) ;
}

$fk = get_flickr_key () ;
$flickr = new phpFlickr ( $fk ) ;

if ( $flickr_stream_url != '' ) {
	$pk = "http://www.flickr.com/photos/" ;
	if ( substr ( $flickr_stream_url , 0 , strlen ( $pk ) ) != $pk ) {
		print "ERROR : Stream URL must begin with $pk" ;
		exit ( 0 ) ;
	}
	$flickr_user = substr ( $flickr_stream_url , strlen ( $pk ) ) ;
	$flickr_user = str_replace ( '/' , '' , $flickr_user ) ;
} else if ( $flickr_user_name != '' ) {
//	$flickr_user = $flickr->people_findByUsername ( $flickr_user_name ) ;
//	$flickr_user = $flickr_user['nsid'] ;
	$flickr_user = $flickr_user_name ;
} else {
	print "Give stream URL or username, dammit!" ;
	exit ( 0 ) ;
}
if ( $test ) print "TEST : NSID $flickr_user<br/>" ;

$fpp = 500 ; // Files per page
$extras = 'license,url_sq,url_o,geo,description,date_taken,owner_name,tags' ;
print "Getting photo batch 1-{$fpp}…<br/>" ; myflush() ;
$files = $flickr->people_getPublicPhotos ( $flickr_user , 3 , $extras , $fpp , 1 ) ;

//print "TESTING, IGNORE: $flickr_user<pre>" ; print_r ( $files ) ; print "</pre>" ;

for ( $page = 2 ; $page <= $files['photos']['pages'] ; $page++ ) {
	print "Getting photo batch " . (($page-1)*$fpp+1) . "-" . ($page*$fpp) . "…<br/>" ; myflush() ;
	$f2 = $flickr->people_getPublicPhotos ( $flickr_user , 3 , $extras , $fpp , $page ) ;
	foreach ( $f2['photos']['photo'] AS $f => $v ) {
		$files['photos']['photo'][] = $v ;
	}
}

foreach ( $words AS $k1 => $v1 ) {
	foreach ( $v1 AS $k2 => $v2 ) {
		$words[$k1][$k2] = explode ( ' ' , $v2 ) ;
	}
}

print "Processing " . count ( $files['photos']['photo'] ) . " images<br/>" ;
print "<table border='1'>" ;
$wq = new WikiQuery ( 'commons' , 'wikimedia' ) ;
$used_names = array () ;
$transferred = 0 ;
foreach ( $files['photos']['photo'] AS $k => $v ) {
	$k2 = $k + 1 ;
	$out = "<tr><td><a href='http://www.flickr.com/photos/{$v['owner']}/{$v['id']}' target='_blank'><img border='0' src='" . $v['url_sq'] . "' /></a></td><td valign='top'>#" . $k2 . " : <b>" . $v['title'] . "</b><br/>" ;
	
	// Check license
	$l = $v['license'] ;
	if ( $l != '4' and $l != '5' and $l != '7' and $l != '8' ) {
		if ( $show_only_uploaded == '' ) print "{$out}Skipping $t - bad license #$l</td></tr>" ;
		myflush();
		continue ;
	}
	
	// Check words
	$ww = array () ;
	$ww['title'] = $v['title'] ;
	$ww['text'] = $v['description'] ;
	$ww['tags'] = $v['tags'] ;
	$ww['either'] = implode ( ' ' , $ww ) ;
	$word_res = '' ;
	foreach ( $ww AS $k2 => $v2 ) {
		$word_res = check_words ( $words[$k2] , $v2 ) ;
		if ( $word_res != '' ) break ;
	}
	if ( $word_res != '' ) {
		if ( $show_only_uploaded == '' ) print "{$out}{$word_res} [for \"{$k2}\"]</td></tr>" ;
		myflush() ;
		continue ;
	}
	
	// Check existence
	$furl = "http://www.flickr.com/photos/" . $v['owner'] . "/" . $v['id'] . "/" ;
	$data = $wq->get_url_usage ( $furl ) ;
	if ( count ( $data ) > 0 ) {
		if ( $show_only_uploaded == 1 ) continue ;
		$u = array () ;
		foreach ( $data AS $d ) {
			$url = get_wikipedia_url ( 'commons' , $d , '' , 'wikimedia' ) ;
			$u[] = "<a href=\"$url\">$d</a>" ;
		}
		print "{$out}This image is already on Commons as " . implode ( ", " , $u ) . "</td></tr>" ;
		myflush();
		continue ;
	}
	
	print $out ;

	// Generate new name
	$new_name = '' ;
	$n = $v['title'] ;
	$n = preg_replace ( '/\.jp[e]{0,1}g$/i' , '' , $n ) ;
	if ( $n == '' ) $n = 'Flickr image #' . $v['id'] ;
	$cnt = 1 ;
	do {
		$new_name = $n ;
		if ( $cnt > 1 ) $new_name .= " ($cnt)" ;
		$new_name .= ".jpg" ;
		$cnt++ ;
	} while ( $wq->does_image_exist ( "Image:" . $new_name ) or $used_names[$new_name] == 1 ) ;
	print "Uploading as <i>$new_name</i>" ;

	// Getting description
	$url = "http://wikipedia.ramselehof.de/flinfo.php?id=" . $v['id'] . "&raw=on&admin=File%20Upload%20Bot%20(Magnus%20Manske)&uploading_user=" . urlencode ( $tusc_user ) ;
	$desc = file_get_contents ( $url ) ;

	if ( stristr($desc,'ramselehof') !== FALSE and stristr($desc,'error') !== FALSE ) {
		print "<a href='http://wikipedia.ramselehof.de/flinfo.php'>FLINFO<a/> error. Not uploading.</td></tr>" ;
		continue;
	}
	
	if ( $desc_add != '' ) $desc = str_replace ( "|Source=" , "<br/>$desc_add\n|Source=" , $desc ) ;
//	if ( $info_add != '' ) $desc = str_replace ( "=={{int:license}}==" , "$info_add\n\n=={{int:license}}==" , $desc ) ;
	if ( $info_add != '' ) $desc = trim ( $desc ) . "\n" . $info_add ;
	
	$desc = trim ( $desc ) ;
	print "<small><pre>" . htmlspecialchars ( $desc ) . "</pre></small>" ;
	
	// FIXME geo
	// FIXME rotation
	
	// Upload
	$url = $v['url_o'] ;
	if ( $test == 1 ) {
		$r = " : TEST, NO UPLOAD : " ;
	} else if ( $simulation == '1' ) {
		$r = " SIMULATION - file not transferred" ;
		$transferred++ ;
	} else {
		$r = do_direct_upload ( $url , $new_name , $desc ) ;
		$transferred++ ;
	}
	$used_names[$new_name] = 1 ;
	
	
	print "$r</td></tr>" ;
	myflush();
	
	if ( $max_transfer != '' and $transferred >= $max_transfer ) {
		print "<tr><td colspan='2'>Tranferred {$max_transfer} images, stopping.</td></tr>" ;
		break ;
	}
}
print "</table>$transferred files transferred!" ;

if ( $simulation == '1' ) print "<br/>SIMULATION - no files were transferred" ;

if ( 0 ) { // Testing
	print "<pre>" ;
	print_r ( $files ) ;
	print "</pre>" ;
}

print "</body></html>" ;

?>