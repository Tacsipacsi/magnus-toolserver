<?PHP

include "common.php" ;

function show_template_data_in_page ( $page ) {
	global $language , $project , $mycon ;
	$page_id = db_get_page_id ( $language , $project , $page ) ;
	//print $page_id ;
	
	
	$ptid = array() ;
	$sql = "SELECT DISTINCT pt_id,tn_name FROM page2template,templatenames WHERE pt_page_id=$page_id AND pt_template_id=tn_id" ;
	$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
	while ( $o = mysql_fetch_object ( $res ) ) $ptid[$o->pt_id] = $o->tn_name ;
	
	
	$sql = "SELECT DISTINCT tn_name,tk_name,kv_value,pt_id FROM templatenames,page2template,templatekeys,templatepagekv WHERE pt_page_id=$page_id AND pt_template_id=tn_id AND pt_id=kv_pt AND tk_id=kv_key ORDER BY tn_name,pt_id,tk_name" ;
	$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
	if ( mysql_errno() != 0 ) { print 'ERROR : ' . mysql_error() . "<br/>" ; return $ret ; } # Some error has occurred
	
	$pretty_page = str_replace ( "_" , " " , $page ) ;
	print "<h2>Templates in \"$pretty_page\"</h2>" ;
	print "<table border='1'>" ;
	print "<tr><th>Template</th><th>Key</th><th>Value</th></tr>" ;

	$d = array();
	while ( $o = mysql_fetch_object ( $res ) ) {
		if ( isset ( $ptid[$o->pt_id] ) ) unset ( $ptid[$o->pt_id] ) ;
		$d[$o->pt_id][] = $o ;
	}

	foreach ( $d AS $v ) {
		$rows = count ( $v ) ;
		
		$first = true ;
		foreach ( $v AS $o ) {
			print "<tr>" ;
			if ( $first ) {
				print "<td rowspan='$rows'>" . $o->tn_name . "</td>" ;
				$first = false ;
			}
			print "<td>" . $o->tk_name . "</td>" ;
			print "<td>" . htmlspecialchars($o->kv_value) . "</td>" ;
			print "</tr>" ;
		}
	}
	
	foreach ( $ptid AS $id => $name ) {
		print "<tr>" ;
		print "<td>" . $name . "</td>" ;
		print "<td colspan='2'><i>No parameters</i></td>" ;
		print "</tr>" ;
	}
	
	print "</table>" ;
}

function show_kvp_containing () {
	global $language , $project , $mycon , $mysql_con ;

	$db = get_db_name ( $language , $project ) ;
	$mysql_con = db_get_con_new($language,$project) ;

	$templates = $_REQUEST["template"] ;
	$keys = $_REQUEST["key"] ;
	$values = $_REQUEST["va"] ;
	$comps = $_REQUEST["comp"] ;
	
	$total = 0 ;
	$page_ids = array () ;
	for ( $cnt = 1 ; $cnt <= 5 ; $cnt++ ) {
		$template = $templates[$cnt] ;
		$key = $keys[$cnt] ;
		$value = $values[$cnt] ;
		$comp = $comps[$cnt] ;
		if ( !isset ( $template ) or $template == '' ) continue ;
//		if ( !isset ( $key ) or $key == '' ) continue ;
//		if ( !isset ( $value ) or $value == '' ) continue ;
		
		make_db_safe ( $template ) ;
		make_db_safe ( $key ) ;
		make_db_safe ( $value ) ;
		
		$template = str_replace ( "_" , " " , $template ) ;
		$key = str_replace ( "_" , " " , $key ) ;
		$value = str_replace ( "_" , " " , $value ) ;
		
		if ( $key == '' and $value == '' ) { // Just presence of template
			$sql = "SELECT DISTINCT pt_page_id FROM page2template,templatenames WHERE pt_template_id=tn_id AND tn_name=\"$template\"" ;
		} else {
			$sql = "SELECT DISTINCT pt_page_id FROM page2template,templatepagekv,templatekeys,templatenames WHERE pt_template_id=tn_id AND tn_name=\"$template\" AND tk_id=kv_key AND tk_name=\"$key\" AND kv_pt=pt_id AND kv_value " ;
			if ( $comp == 'equal' ) $sql .= "= \"$value\"" ;
			else $sql .= "LIKE \"%$value%\" " ;
		}
		
//		print "$sql<br/>" ;
		$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
		if ( mysql_errno() != 0 ) { print 'ERROR : ' . mysql_error() . "<br/>" ; exit ; }
		while ( $o = mysql_fetch_object ( $res ) ) {
			$page_ids[$o->pt_page_id]++ ;
		}
		$total++ ;
	}

	foreach ( $page_ids AS $k => $v ) {
		if ( $v < $total ) unset ( $page_ids[$k] ) ;
	}
	
	$sql = "SELECT page_id,page_title,page_namespace FROM page WHERE page_id IN (" . implode ( "," , array_keys ( $page_ids ) ) . ")" ;
//	print "$sql<br/>" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) { print 'ERROR : ' . mysql_error() . "<br/>" ; exit ; }
	
	print "<h2>Results</h2><ol>" ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$title = $o->page_title ;
		$title = str_replace ( '_' , ' ' , $title ) ;
		$ut = myurlencode ( $title ) ;
		// http://toolserver.org/~magnus/knights_template.php?mode=show_template_data_in_page&page=Roy+Scheider
		print "<li>" ;
		print "<a href='http://$language.$project.org/wiki/$ut'>$title</a> (see <a href='./knights_template.php?mode=show_template_data_in_page&page=$ut&doit=1'>all templates on that page</a>)" ;
		print "</li>" ;
	}
	print "</ol>" ;
}

function show_stats () {
	global $mycon;
	
	print "<ul>" ;
	
	$sql = "SELECT count(DISTINCT pt_page_id) AS d FROM page2template" ;
	$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
	if ( mysql_errno() != 0 ) print 'ERROR : ' . mysql_error() . "<br/>" ; 
	$o = mysql_fetch_object ( $res ) ;
	print "<li>" . $o->d . " pages indexed</li>" ;

	$sql = "SELECT count(*) AS d FROM templatenames" ;
	$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
	if ( mysql_errno() != 0 ) print 'ERROR : ' . mysql_error() . "<br/>" ; 
	$o = mysql_fetch_object ( $res ) ;
	print "<li>" . $o->d . " distinct templates used</li>" ;

	$sql = "SELECT count(*) AS d FROM templatekeys" ;
	$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
	if ( mysql_errno() != 0 ) print 'ERROR : ' . mysql_error() . "<br/>" ; 
	$o = mysql_fetch_object ( $res ) ;
	print "<li>" . $o->d . " distinct template keys</li>" ;

	$sql = "SELECT count(*) AS d FROM templatepagekv" ;
	$res = mysql_db_query ( 'u_magnus_templates_en' , $sql , $mycon ) ;
	if ( mysql_errno() != 0 ) print 'ERROR : ' . mysql_error() . "<br/>" ; 
	$o = mysql_fetch_object ( $res ) ;
	print "<li>" . $o->d . " total key/value pairs</li>" ;
	
	print "</ul>" ;
}

function show_main_form () {
	global $mode ;
	$stdip = $mode == 'show_template_data_in_page' ? ' checked' : '' ;
	show_stats () ;
	print "<form method='get' action='knights_template.php'>" ;
	print "<table border='1'>" ;
	print "<tr><th><input type='radio' name='mode' value='show_template_data_in_page' $stdip />Show template data in a page</th><td><input type='text' name='page' value='$page' /></td></tr>" ;
	
	print "<tr><th align='left' valign='top'><input type='radio' name='mode' value='show_kvp_having' />Key/values</th><td>" ;
	for ( $cnt = 1 ; $cnt <= 5 ; $cnt++ ) {
		if ( $cnt == 1 ) print 'H' ;
		else print 'h' ;
		print "as template <input type='text' name='template[$cnt]' value='' /> where key <input type='text' name='key[$cnt]' value='' /> " ;
		print "<input type='radio' name='comp[$cnt]' value='equal' />equals " ;
		print "<input type='radio' name='comp[$cnt]' value='contain' checked />contains " ;
		print "<input type='text' name='va[$cnt]' value='' />" ;
		if ( $cnt < 5 ) print " and" ;
		print "<br/>" ;
	}
	print "<small><i>Note : </i> Leave key/value blank to check for presence of a template</small>" ;
	print "</td></tr>" ;
	
	print "<tr><td /><td><input type='submit' name='doit' value='Do it' /></td></tr>" ;
	print "</table>" ;
	print "</form>" ;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$language = 'en' ;
$project = 'wikipedia' ;
$page = str_replace ( " " , "_" , get_request ( 'page' , '' ) ) ;
$mode = get_request ( 'mode' , 'show_template_data_in_page' ) ;
$doit = isset ( $_REQUEST['doit'] ) ;


get_database_password() ;
$server = 'sql-s1' ;
$user = 'magnus' ;


if ( !@$mycon = mysql_connect ( $server , $user , $mysql_password ) ) {
	echo 'Could not connect to mysql : ' ;
	print mysql_error();
	exit ;
}


print "<html>" ;
print "<head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"></head>" ;
print "<body>" ;

//$page = 'Roy_Scheider' ;
//$page = 'Magnetic_field' ;

if ( $doit ) {
	if ( $mode == 'show_template_data_in_page' ) show_template_data_in_page ( $page ) ;
	if ( $mode == 'show_kvp_having' ) show_kvp_containing () ;
} else {
	show_main_form () ;
}


//print "OK" ;
print "</body>" ;
print "</html>" ;

?>