<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

if ( isset ( $_REQUEST['kml'] ) ) {
	$hide_header = true ;
	$hide_doctype = true ;
}

require_once ( 'php/common.php' ) ;
require_once ( '../../geohack/public_html/geo_param.php' ) ;

$geohack_key = '//tools.wmflabs.org/geohack/geohack.php' ;

$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$namespace = get_request ( 'namespace' , 0 ) ;
$title = get_request ( 'title' , '' ) ;

$db = openDB ( $language , $project ) ;

if ( isset ( $_REQUEST['kml'] ) and $title != '' ) {
	$kml = "<?xml version='1.0' encoding='UTF-8'?>\n<kml xmlns='https://www.opengis.net/kml/2.2'>\n<Document>\n" ;
	$kml .= "<name>$title</name>\n<open>1</open>\n<description>Places related to $title (namespace $namespace) from $language.$project.</description>\n<Folder>\n" ;

	make_db_safe ( $language ) ;
	make_db_safe ( $project ) ;
	$language = strtolower ( $language ) ;
	$project = strtolower ( $project ) ;
	
	$stitle = get_db_safe ( $title ) ;
	make_db_safe ( $namespace ) ;
	$pid = 0 ;
	$sql = "select page_id from page where page_title=\"$stitle\" and page_namespace=\"$namespace\"" ;

	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$pid = $o->page_id ;
	}
	if ( $pid == 0 ) {
		print "<p>Page not found : $title</p>" ;
	} else {
		$pids = array () ;
		$sql = "select DISTINCT page_id from page,pagelinks where pl_from=$pid and pl_namespace=page_namespace and pl_title=page_title" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$pids[] = $o->page_id ;
		}
		$pids = implode ( ',' , $pids ) ;
		$sql = "select distinct page_title,el_to from page,externallinks where el_from in ($pids) and el_from=page_id and page_namespace=0" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			if ( substr ( $o->el_to , 0 , strlen ( $geohack_key ) ) != $geohack_key ) continue ;
			$param = array_pop ( explode ( '&params=' , $o->el_to ) ) ;
			$gp = new geo_param ( $param ) ;
			$id = urlencode ( $o->page_title ) ;
			$pt = str_replace ( '_' , ' ' , $o->page_title ) ;
			$desc = "https://$language.$project.org/wiki/" . $o->page_title ;
			$kml .= "<Placemark id='$id'>\n<name>$pt</name>\n<description>$desc</description>\n<Point>\n<coordinates>{$gp->londeg},{$gp->latdeg},0</coordinates>\n</Point>\n</Placemark>\n" ;
//			print $o->page_title . " : " . $param . " : " . $gp->latdeg . "/" . $gp->londeg . "<br/>" ;
		}
	}
	
	$kml .= "</Folder></Document>\n</kml>" ;
//	print "<pre>" . htmlentities ( $kml ) . "</pre>" ;
	header("Content-Type: application/xml; charset=UTF-8");
	print $kml ;
	exit ( 0 ) ;
}


function print_form () {
	global $language , $project , $namespace , $title ;
	$ns = array () ;
	$ns[0] = '(Article)' ;
	$ns[2] = 'User' ;
		
	print "<form method='get' action='./related_places.php' class='form form-inline'>
	<table border='1'>
	<tr><th>Language</th><td><input type='text' name='language' value='$language' /></td></tr>
	<tr><th>Project</th><td><input type='text' name='project' value='$project' /></td></tr>
	<tr><th>Namespace</th><td>
	<select name='namespace'>" ;
	
	foreach ( $ns AS $num => $txt ) {
		$checked = $namespace == $num ? 'selected' : '' ;
		print "<option value='$num' $checked>$txt</option>" ;
	}
	
	print "</select>
	</td></tr>
	<tr><th>Title</th><td><input type='text' name='title' value='$title' /></td></tr>
	<tr><td colspan='2' align='right'><input type='submit' name='doit' value='Do it!' class='btn btn-primary' /></td></tr>
	</table>
	</form>" ;
}

print get_common_header ( 'related_places.php' , 'Related places' ) ;

if ( isset ( $_REQUEST['doit'] ) ) {
	$url = "https://tools.wmflabs.org/magnus-toolserver/related_places.php?kml=1&language=$language&project=$project&namespace=$namespace&title=" . urlencode ( $title ) ;
	$url = 'https://maps.google.com/maps?f=q&hl=en&q=' . urlencode ( $url ) ;
	print "<p>See <a href=\"$url\">the map</a> with the locations.</p>" ;
}

print_form () ;

print "</body></html>" ;

?>