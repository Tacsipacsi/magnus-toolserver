<?php

error_reporting ( E_ALL ) ;

include_once ( "php/wikiquery.php") ;
include_once ( "php/legacy.php") ;

@set_time_limit ( 20*60 ) ; # Time limit 20 min


#________________________________________________________________________________________________________________________

function add_redirect ( $from , $lang , &$redirects ) {
    if ( !isset ( $redirects[$from] ) ) $redirects[$from] = array () ;
    $redirects[$from][$lang] = $lang ;
}


function db_get_language_links ( $title , $language , $project ) {
	global $db ;
	make_db_safe ( $title ) ;
	
	$ret = array () ;
	$sql = "SELECT  * FROM langlinks,page WHERE ll_from=page_id AND page_title=\"{$title}\" AND page_namespace=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[$o->ll_lang] = $o->ll_title ;
	}
	return $ret ;	
}


#________________________________________________________________________________________________________________________

$bigones = array ( 'en' , 'de' , 'fr' , 'pl' , 'ja' , 'nl' , 'it' , 'pt' , 'es' , 'sv' ) ;
asort ( $bigones ) ;

$language = fix_language_code ( get_request ( 'language' , 'de' ) ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$depth = get_request ( 'depth' , 0 ) ;
$category = get_request ( 'category' , '' ) ;
$hidezero = isset ( $_REQUEST['hidezero'] ) ;
$doit = isset ( $_REQUEST['doit'] ) ;
if ( !$doit ) $hidezero = 1 ; # Default
$hzchk = $hidezero ? 'checked' : '' ;

# Header
print get_common_header ( "redirector.php" , 'Redirector' ) ;
print "Scan a category tree, find language links, find redirects to those, and check if these redirects are also wanted in the original project. Duh!<br/>" ;
print "<i>Currently checks " . implode ( ", " , $bigones ) . " wikipedias.</i><br/>" ;
print "<small>Output format : Link to existing redirect or button to create new redirect ; number of times the redirect is linked to in the original wiki ; languages that have an article or redirect under that name.</small>" ;

print "<form method='post' taget='pages_in_cats.php' class='form inline-form form-inline'>
<table>
<!--<tr><th>Project</th><td><input type='text' name='project' value='{$project}'/></td></tr>-->
<tr><th>Language</th><td><input type='text' name='language' value='{$language}'/></td></tr>
<tr><th>Category</th><td><input type='text' name='category' value='{$category}'/></td></tr>
<tr><th>Depth</th><td><input type='text' name='depth' value='{$depth}'/></td></tr>
<tr><th></th><td><input type='checkbox' name='hidezero' value=1 {$hzchk}/>Hide redirects that are not wanted</td></tr>
<tr><th></th><td><input type='submit' name='doit' value='Run' class='btn btn-primary' /></td></tr>
</table></form>" ;

if ( !$doit ) {
  print get_common_footer() ;
  exit ;
}

$wq = array () ;
foreach ( $bigones AS $b ) $wq[$b] = new WikiQuery ( $b , $project ) ;
if ( !isset ( $wq[$language] ) ) $wq[$language] = new WikiQuery ( $language , $project ) ;

//$pages_orig = db_get_articles_in_category ( $language , $category , $depth ) ;
$db = openDB ( $language , $project ) ;
$pages_orig = getPagesInCategory ( $db , $category , $depth ) ;

//print "<pre>" ; print_r ( $pages_orig ) ; print "</pre>" ; exit ( 0 ) ;

print "Scanning " . count ( $pages_orig ) . " articles..." ; myflush();
print "<table border='1'>" ;

foreach ( $pages_orig AS $page ) {
  $pretty_page = str_replace ( '_' , ' ' , $page ) ;
  $lls = db_get_language_links ( $page , $language , $project ) ;
  
  $redirects = array () ;
  foreach ( $lls AS $k => $ll ) {
    if ( !in_array ( $k , $bigones ) ) continue ;
    add_redirect ( $ll , $k , $redirects ) ;
    
    $res = $wq[$k]->get_backlinks_api ( $ll , 'redirects' ) ;
    foreach ( $res AS $re ) add_redirect ( $re , $k , $redirects ) ;
  }
  unset ( $redirects[$page] ) ;
  
  $existing = $wq[$language]->get_existing_pages ( array_keys ( $redirects ) ) ;
  
  $recount = array () ;
  foreach ( $redirects AS $repage => $v ) {
    $bls = $wq[$language]->get_backlinks_api ( $repage ) ;
    $recount[$repage] = count ( $bls ) ;
  }
  ksort ( $recount ) ;
  arsort ( $recount ) ;
  
  if ( $hidezero ) {
    foreach ( $recount AS $k => $v ) {
      if ( $v == 0 ) unset ( $recount[$k] ) ;
    }
  }
  
  print "<tr>" ;
  print "<th valign='top'><a target='_blank' href=\"http://$language.$project.org/wiki/$page\">$pretty_page</a></th>" ;

  if ( count ( $recount ) == 0 ) {
    print "<td colspan='2'><i>No redirects wanted!</i></td></tr>" ;
    myflush();
    continue ;
  }

  
  print "<td valign='top'>Non-existing redirects:<ul>" ;
  foreach ( $recount AS $k => $v ) {
    if ( in_array ( $k , $existing ) ) continue ;
    print "<li>" ;
    $text = "#REDIRECT [[$pretty_page]]" ;
    $summary = "$text (http://tools.wikimedia.de/~magnus/redirector.php)" ;
    $bt = "Redirect \"$k\" to \"$pretty_page\"" ;
    print cGetEditButton ( $text , $k , $language , $project , $summary , $bt , true , false , false , true ) ;

    print " ($v&times;) <i>" . implode ( ", " , $redirects[$k] ) . "</i></li>" ;
  }
  print "</ul></td>" ;
  
  print "<td valign='top'>Existing redirects:<ul>" ;
  foreach ( $recount AS $k => $v ) {
    if ( !in_array ( $k , $existing ) ) continue ;
    print "<li><a target='_blank' href=\"http://$language.$project.org/wiki/$k\">$k</a>" ;
    print " ($v&times;) <i>" . implode ( ", " , $redirects[$k] ) . "</i></li>" ;
  }
  print "</ul></td>" ;
  
  print "</tr>" ;
  myflush() ;
}

print "</table>All done!" ;
print get_common_footer() ;

?>