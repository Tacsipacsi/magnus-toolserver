<?PHP

error_reporting ( E_ALL ) ;

include_once ( "common.php" ) ;

print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' ;
print "</head><body>" ;
print get_common_header ( "openlibrary_authors.php" ) . "\n" ;
myflush() ;

$language = 'en' ;
$project = 'wikipedia' ;
$category = 'Writers by award' ;
$depth = 1 ;

$articles = db_get_articles_in_category ( $language , $category , $depth , 0 , $x , true , '' , $project , false ) ;
//$has_ol_author = db_get_template_usage ( $language , "OL author" , 0 , $project ) ;

shuffle ( $articles ) ;
while ( 1 ) {
	if ( count ( $articles ) == 0 ) {
		print "No more articles left!" ;
		exit () ;
	}
	$article = array_pop ( $articles ) ;
	$ts = db_get_used_templates ( $language , $article , 0 , $project , false ) ;
	if ( in_array ( $ts , "OL_author" ) ) continue ;

	
	$pa = str_replace ( '_' , ' ' , $article ) ;
	$out = '' ;
	$out .= "<a href=\"http://$language.$project.org/wiki/$article\" target='_blank'>$pa</a> | " ;
	$out .= "<a href=\"http://$language.$project.org/w/index.php?title=$article&action=edit\" target='_blank'>Edit</a><br/>" ;
	
	
	$cats = get_categories_of_page ( $language , $project , $article , 0 ) ;
	ksort ( $cats ) ;
	$years = array () ;
	foreach ( $cats AS $c => $dummy ) {
		$c2 = explode ( '_' , $c ) ;
		if ( count ( $c2 ) != 2 ) continue ;
		if ( $c2[1] == 'births' || $c2[1] == 'deaths' ) {
			$out .= "<b>" . str_replace ( '_' , ' ' , $c ) . "</b><br/>" ;
			$years[] = $c2[0] ;
		}
	}
	//print "<pre>" ; print_r ( $cats ) ; print "</pre>" ;
	
	$ol_url = "http://openlibrary.org/search/authors?q=" . urlencode ( $pa ) ;
	$html = file_get_contents ( $ol_url ) ;
	$html = array_pop ( explode ( "authorList" , $html , 2 ) ) ;
	$html = array_shift ( explode ( '</ul>' , $html ) ) ;
	$html = explode ( '</li>' , $html ) ;
	array_pop ( $html ) ;
	
	if ( count ( $html ) == 0 ) continue ;
	
	$keys = array () ;
	print "$out<table border='1'>" ;
	foreach ( $html AS $part ) {
		$part = explode ( 'href="' , $part , 2 ) ;
		$href = array_pop ( $part ) ;
		$rest = explode ( '"' , $href , 2 ) ;
		$href = array_shift ( $rest ) ;
		$rest = array_pop ( $rest ) ;
		$rest = array_pop ( explode ( '>' , $rest , 2 ) ) ;
		$rest = explode ( '</a>' , $rest , 2 ) ;
		$name = array_shift ( $rest ) ;
		$desc = trim ( str_replace ( '&nbsp;' , ' ' , array_pop ( $rest ) ) ) ;
		$key = array_pop ( explode ( '/' , $href ) ) ;
		foreach ( $years AS $y ) {
			$desc = str_replace ( $y , "<span style='color:red'>$y</span>" , $desc ) ;
		}
		print "<tr><th><a href='http://openlibrary.org/authors/$key' target='_blank'>$name</a></th><td><tt>== External links ==<br/>* {{OL author|$key}}</tt><br/>$desc</td></tr>" ;
		$keys[] = "key=$key" ;
		
	}
	print "</table>" ;
	
	print "<a href='$ol_url' target='_blank'>View search</a> | " ;
	$url = "http://openlibrary.org/authors/merge?" . implode ( '&' , $keys ) ;
	print "<a href='$url' target='_blank'>Merge authors</a> | <a href='./openlibrary_authors.php'>NEXT</a><br/><br/>" ;
	print "<iframe src=\"http://$language.$project.org/wiki/$article\" width='100%' height=800></iframe>" ;
	break ;
}

print "</body></html>" ;
?>
