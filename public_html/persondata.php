<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( 'php/common_data.php' ) ;
include_once ( 'php/wikiquery.php' ) ;
include_once ( 'php/legacy.php' ) ;

$persondata_list = array (
	'de' => 'Personendaten' ,
	'en' => 'Persondata' ,
) ;
$persondata_text = array (
	'de' => "|NAME=$1\n|ALTERNATIVNAMEN=$2\n|KURZBESCHREIBUNG=$3\n|GEBURTSDATUM=$4\n|GEBURTSORT=$5\n|STERBEDATUM=$6\n|STERBEORT=$7" ,
	'en' => "|NAME=$1\n|ALTERNATIVE NAMES=$2\n|SHORT DESCRIPTION=$3\n|DATE OF BIRTH=$4\n|PLACE OF BIRTH=$5\n|DATE OF DEATH=$6\n|PLACE OF DEATH=$7" ,
) ;
$category_name = array (
	'de' => 'Kategorie' ,
	'en' => 'Category' ,
) ;
$born_cat = array (
	'de' => 'Geboren $1' ,
	'en' => '$1 births' ,
) ;
$died_cat = array (
	'de' => 'Gestorben $1' ,
	'en' => '$1 deaths' ,
) ;
$bad_start = array (
	'en' => array ( 'is ' , 'was ' , 'a ' , 'an ' ) ,
	'de' => array ( 'ist ' , 'war ' , 'ein ' , 'eine ' , 'der ' , 'die ' ) ,
) ;



$months = array (
	'de' => array ('januar','februar','märz','april','mai','juni','juli','august','september','oktober','november','dezember') ,
	'en' => array ('january','february','march','april','may','june','july','august','september','october','november','december') ,
) ;
$list_of_days = array () ;

function cut_start ( &$text , $start ) {
	if ( substr ( $text , 0 , strlen ( $start ) ) != $start ) return ;
	$text = trim ( substr ( $text , strlen ( $start ) ) ) ;
}

function extract_date_place ( $indate , &$year , &$day , &$place , &$date ) {
	global $language ;
	$function = 'extract_date_place_' . $language ;
	$function ( $indate , &$year , &$day , &$place , &$date ) ;
}

function extract_date_place_de ( $indate , &$year , &$day , &$place , &$date ) {
	global $list_of_days , $months , $language ;
	$indate = " " . $indate . " " ;
	$indate = trim ( $indate ) ;
	
	$ldate = strtolower ( $indate ) ;
	
	# Initialize list of days
	if ( count ( $list_of_days ) == 0 ) {
		foreach ( $months[$language] AS $month ) {
			for ( $a = 31 ; $a >= 1 ; $a-- ) { # So "11." comes before "1."
				$list_of_days[] = $a . '. ' . $month ;
			}
		}
	}
	
	# Find day
	$day = '' ;
	foreach ( $list_of_days AS $oneday ) {
		if ( false === strpos ( $ldate , $oneday ) ) continue ;
		$day2 = explode ( '. ' , $oneday ) ;
		$day = array_shift ( $day2 ) . '. ' ;
		$day .= ucfirst ( array_shift ( $day2 ) ) ;
		break ;
	}
	
	# Find year
	if ( 0 < preg_match ( '/[0-9]{3,4}/' , $indate , $year ) ) {
		$year = array_shift ( $year ) ;
	}
	
	# Place
	if ( $year != '' ) {
		$place = array_pop ( explode ( $year , $indate ) ) ;
		while ( $place != '' && $place[0] == ']' ) $place = substr ( $place , 1 ) ;
		$place = trim ( $place ) ;
		cut_start ( $place , 'in ' ) ;
		cut_start ( $place , ',' ) ;
	}
	
	# Set date
	if ( $year == '' ) return ;
	if ( $day == '' ) $date = '[[' . $year . ']]' ;
	else $date = '[[' . $day . ']] [[' . $year . ']]' ;
	if ( is_array ( $date ) ) $date = '' ;
}

function extract_date_place_en ( $indate , &$year , &$day , &$place , &$date ) {
	global $list_of_days , $months , $language ;
	$indate = " " . $indate . " " ;
	$indate = str_replace ( ' born ' , '' , $indate ) ;
	$indate = str_replace ( ' died ' , '' , $indate ) ;
	$indate = trim ( $indate ) ;
	
	$ldate = strtolower ( $indate ) ;
	
	$list_of_days2 = array () ;
	
	# Initialize list of days
	if ( count ( $list_of_days ) == 0 ) {
		foreach ( $months[$language] AS $month ) {
			for ( $a = 31 ; $a >= 1 ; $a-- ) {
				$list_of_days[] = $month . ' ' . $a ;
				$list_of_days2[$month . ' ' . $a] = $a . ' ' . $month ;
			}
		}
	}
	
	# Find day
	$day = '' ;
	foreach ( $list_of_days AS $oneday ) {
		if ( false === strpos ( $ldate , $oneday ) ) continue ;
		$day = ucfirst ( $oneday ) ;
		break ;
	}
	if ( $day == '' ) {
    foreach ( $list_of_days2 AS $k => $oneday ) {
      if ( false === strpos ( $ldate , $oneday ) ) continue ;
      $day = ucfirst ( $k ) ;
      break ;
    }
  }
	
	# Find year
	if ( 0 < preg_match ( '/[0-9]{3,4}/' , $indate , $year ) ) {
		$year = array_shift ( $year ) ;
	}
	
	# Place
	if ( $year != '' ) {
		$place = array_pop ( explode ( $year , $indate ) ) ;
		while ( $place != '' && $place[0] == ']' ) $place = substr ( $place , 1 ) ;
		$place = trim ( $place ) ;
		cut_start ( $place , 'in ' ) ;
		cut_start ( $place , ',' ) ;
#		if ( substr ( $place , 0 , 3 ) == 'in ' ) $place = trim ( substr ( $place , 3 ) ) ;
#		if ( substr ( $place , 0 , 1 ) == ',' ) $place = trim ( substr ( $place , 1 ) ) ;
	}
	
#	print $day . ":" . $indate . " " ;
	
	# Set date
	if ( $year == '' ) return ;
	if ( $day == '' ) $date = $year ;
	else $date = $day . ', ' . $year ;
	
	if ( is_array ( $date ) ) $date = $year = '' ;
}

function fix_text_end ( &$text ) {
	while ( substr ( $text , -1 , 1 ) == ',' OR substr ( $text , -1 , 1 ) == ';' ) {
		$text = trim ( substr ( $text , 0 , strlen ( $text ) - 1 ) ) ;
	}
	$text = trim ( $text ) ;
}

function remove_bad_starts ( &$text ) {
	global $bad_start , $language ;
	do {
		$otext = $text ;
		foreach ( $bad_start[$language] AS $bad ) {
			cut_start ( $text , $bad ) ;
		}
		$text = trim ( $text ) ;
	} while ( $text != $otext ) ;
	fix_text_end ( $text ) ;
}

function get_unbraced ( $t , $k ) {
  $b = 0 ;
  $p = 0 ;
  while ( $p < strlen ( $t ) AND ( $b > 0 OR false === stripos ( $k , $t[$p] ) ) ) {
    if ( false !== stripos ( '{[(' , $t[$p] ) ) $b++ ;
    if ( false !== stripos ( ')]}' , $t[$p] ) ) $b-- ;
    $p++ ;
  }
  return substr ( $t , 0 , $p ) ;
}

function next_thing ( $text , $key , $sep ) {
  $s = split ( $key , $text , 2 ) ;
  if ( count ( $s ) > 1 ) {
    $s = trim ( get_unbraced ( array_pop ( $s ) , $sep ) ) ;
    return $s ;
  }
  return '' ;
}


# Header
print get_common_header ( "persondata.php" , 'Persondata' ) ;

$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$title = get_request ( 'title' , '' ) ;
$depth = get_request ( 'depth' , '3' ) ;
$category = get_request ( 'category' , '' ) ;
$emptyskip = get_request ( 'emptyskip' , false ) ;
$infobox_person = isset ( $_REQUEST['infobox_person'] ) ;
$silentskip = isset ( $_REQUEST['silentskip'] ) ;
if ( !isset ( $_REQUEST['doit'] ) ) $silentskip = true ;
if ( !isset ( $_REQUEST['doit'] ) ) $infobox_person = true ;

$sil = $silentskip ? 'checked' : '' ;
$emp = $emptyskip ? 'checked' : '' ;
$gip = $infobox_person ? 'checked' : '' ;

print "
<form method='post' class='form form-inline'>
<table>
<tr><th>Language</th><td><input type='text' name='language' value='{$language}'/></td></tr>
<tr><th>Project</th><td><input type='text' name='project' value='{$project}'/></td></tr>
<tr><th>Title</th><td><input type='text' name='title' value='{$title}'/> <i>or</i></td></tr>
<tr>
<th>Category</th><td><input type='text' name='category' value='{$category}'/></td>
<th>Depth</th><td><input type='text' name='depth' value='{$depth}'/></td>
</tr>
<tr><th colspan='3'>Generate {{Infobox Person}} (EN only)</th><td><input type='checkbox' name='infobox_person' value='1'{$gip}/></td></tr>
<tr><th colspan='3'>Only articles with missing persondata</th><td><input type='checkbox' name='silentskip' value='1'{$sil}/></td></tr>
<tr><th colspan='3'>Only articles with at least two dates/place</th><td><input type='checkbox' name='emptyskip' value='1'{$emp}/></td></tr>
</table>
<input type='submit' name='doit' value='Do it' class='btn btn-primary' />
</form>
" ;

$persondata = $persondata_list[$language] ;
$l_persondata = strtolower ( $persondata ) ;

$q = new WikiQuery ( $language , $project ) ;

$db = openDB ( $language , $project ) ;

$titles = array () ;
if ( $title != '' ) $titles[] = $title ; # Single title
else if ( $category != '' ) { # Get category pages
	print "Getting page list for category \"{$category}\" on {$language}.{$project}.org ... " ;
	myflush() ;
//	$titles = db_get_articles_in_category ( $language , $category , $depth ) ;
	$titles = getPagesInCategory ( $db , $category , $depth , 0 , true ) ;
#	$titles = array_keys ( $q->get_pages_in_category ( $category , 0 , $depth ) ) ;
/*
	print "Getting page list for category \"{$category}\" on {$language}.{$project}.org via CatScan ... " ;
	myflush() ;
	$ot = get_catscan_pages ( $language , $category , $depth , $project ) ;
	print "!" ; myflush () ;
	foreach ( $ot AS $t ) {
		if ( $t->namespace != '0' ) continue ;
		$titles[] = $t->title ;
	}*/
	print 'done (' . count ( $titles ) . ' titles found).<br/>' ;
	myflush () ;
} else exit ;

#print "TESTING - Do NOT USE!<br/>" ;

$is_on_toolserver = false ; # Strange speedup

foreach ( $titles AS $title ) {
	# Determine persondata status
	$templates = $q->get_used_templates ( $title ) ;
	#print "!" . implode ( ", " , $templates ) . "<br/>" ;
	if ( in_array ( $persondata , $templates ) ) {
		if ( !$silentskip ) {
			print "<p>Skipping \"{$title}\" - already has {{" . $persondata . "}}</p>" ;
			myflush() ;
		}
		continue ;
	}
	
	# Get text
	$text = get_wikipedia_article ( $language , $title , true , $project ) . "\n" ;
	$text = str_replace ( '&nbsp;' , ' ' , $text ) ;
	$ltext = strtolower ( $text ) ;
	$ip = get_initial_paragraph ( $text ) ;
	$ip = preg_replace ( "/\{\{[^\}]+\}\};*\s*/" , "" , $ip ) ; # Removing any templates
	$ip = str_replace ( '[[Circa|c]]' , '' , $ip ) ;
	
	# Name
	$name = str_replace ( '_' , ' ' , $title ) ;
	$name = trim ( array_shift ( explode ( '(' , $name , 2 ) ) ) ;
	$n = explode ( ' ' , $name ) ;
	$inverted_name = array_pop ( $n ) ;
	if ( trim ( str_replace ( '.' , '' , str_replace ( 'I' , '' , $inverted_name ) ) ) == '' ) {
		$inverted_name = $name ; # "Someone II."
	} else {
		$inverted_name .= ', ' ;
		$inverted_name .= implode ( ' ' , $n ) ;
	}
	
	# Dates
	$dates = array_pop ( explode ( '(' , $ip , 2 ) ) ;
	$dates = array_shift ( explode ( ')' , $dates , 2 ) ) ;
	$birth_year = '' ;
	$death_year = '' ;
	$birth_day = '' ;
	$death_day = '' ;
	$birth_place = '' ;
	$death_place = '' ;
	$birth_date = '' ;
	$death_date = '' ;
	$d = explode ( '&mdash;' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( ', died ' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( '&ndash;' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( ' - ' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( ']]-' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( ';' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( '/' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( '–' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( '†' , $dates ) ;
	if ( count ( $d ) != 2 ) $d = explode ( '&dagger;' , $dates ) ;
	
	if ( count ( $d ) == 2 ) {
		extract_date_place ( array_shift ( $d ) , $birth_year , $birth_day , $birth_place , $birth_date ) ;
		extract_date_place ( array_shift ( $d ) , $death_year , $death_day , $death_place , $death_date ) ;
	} else extract_date_place ( array_shift ( $d ) , $birth_year , $birth_day , $birth_place , $birth_date ) ;

	# Fixes
	if ( $death_year != '' AND $death_year < 1400 ) $inverted_name = $name ; # Middle ages correction hack
	if ( $death_place == 'ebenda' ) $death_place = $birth_place ; # DE: hack
	if ( $death_place == 'ebd.' ) $death_place = $birth_place ; # DE: hack
	fix_text_end ( $birth_place ) ;
	fix_text_end ( $birth_date ) ;
	fix_text_end ( $death_place ) ;
	fix_text_end ( $death_date ) ;
	
	if ( $emptyskip ) {
		$cnt = 0 ;
		if ( $birth_place != '' ) $cnt++ ;
		if ( $birth_date != '' ) $cnt++ ;
		if ( $death_place != '' ) $cnt++ ;
		if ( $death_date != '' ) $cnt++ ;
		if ( $cnt < 2 ) continue ;
	}

	# Description
	$desc = array_pop ( explode ( ')' , $ip , 2 ) ) ;
	
	$cnt = 0 ;
	for ( $a = 0 ; $a < strlen ( $desc ) AND ( $cnt != 0 OR $desc[$a] != '.' ) ; $a++ ) {
		if ( $desc[$a] == '(' OR $desc[$a] == '[' OR $desc[$a] == '{' ) $cnt++ ;
		if ( $desc[$a] == ')' OR $desc[$a] == ']' OR $desc[$a] == '}' ) $cnt-- ;
	}
	$desc = substr ( $desc , 0 , $a ) ;
	cut_start ( $desc , ',' ) ;
	
	remove_bad_starts ( $desc ) ;
	remove_bad_starts ( $birth_place ) ;
	remove_bad_starts ( $death_place ) ;
	
	# URL
	$url = get_wikipedia_url ( $language , $title , "" , $project ) ;
	
	# EN special
	if ( $language == 'en' ) {
    if ( $birth_place == '' ) $birth_place = next_thing ( $text , ' born in ' , ',.;' ) ;
    if ( $birth_place == '' ) $birth_place = next_thing ( $text , ' born at ' , ',.;' ) ;
    if ( $death_place == '' ) $death_place = next_thing ( $text , ' died in ' , ',.;' ) ;
	}
	
	# Additional
	$additional = array () ;
	
	if ( $name != $inverted_name ) {
    $defaultsort = '{{DEFAULTSORT:' . $inverted_name . '}}' ;
    if ( false === strpos ( $ltext , strtolower ( $defaultsort ) ) ) {
      $additional[] = $defaultsort ;
    }
    
    
		$cat_start = "[[{$category_name[$language]}:" ;
		$cat_end = "|{$inverted_name}]]" ;
		
		$cat = $cat_start . str_replace ( '$1' , $birth_year , $born_cat[$language] ) . $cat_end ;
		$cat_brief = $cat_start . str_replace ( '$1' , $birth_year , $born_cat[$language] ) . "]]" ;
		if ( false === strpos ( $ltext , strtolower ( $cat ) ) AND false === strpos ( $ltext , strtolower ( $cat_brief ) ) AND $birth_year != '' ) {
			$additional[] = $cat_brief ;
		}
		
		$cat = $cat_start . str_replace ( '$1' , $death_year , $died_cat[$language] ) . $cat_end ;
		$cat_brief = $cat_start . str_replace ( '$1' , $death_year , $died_cat[$language] ) . "]]" ;
		if ( false === strpos ( $ltext , strtolower ( $cat ) ) AND false === strpos ( $ltext , strtolower ( $cat_brief ) ) AND $death_year != '' ) {
			$additional[] = $cat_brief ;
		}
	}
	
	$additional = implode ( '<br/>' , $additional ) ;
	if ( $additional != '' ) $additional .= "<br/>" ;
	
	# Generate {{Infobox Person}}
	$infobox_person_text = '' ;
	if ( $language == 'en' AND $infobox_person AND false === strpos ( $ltext , strtolower ( 'infobox person' ) ) ) {
    $image = '' ;
    $image_size = '' ;
    $image_caption = '' ;
    if ( 0 < preg_match ( '/\[\[Image:([^\|]+)/i' , $text , $data ) ) {
      $image = $data[1] ;
      if ( 0 < preg_match ( '/\|\s*(\d+px)\s*\|/i' , $text , $data2 ) ) $image_size = $data2[1] ;
      if ( 0 < preg_match ( '/\[\[Image:[^\|]+\|([^\]]+)\]\]/i' , $text , $data2 ) ) {
        $image_caption = trim ( array_pop ( explode ( '|' , $data2[1] ) ) ) ;
        if ( $image_caption == $name ) $image_caption = '' ; # No need to repeat the name
      }
    }
	
    $infobox_person_text = "{{Infobox Person
| name        = $name
| image       = $image
| image_size  = $image_size
| caption     = $image_caption
| birth_date  = $birth_date
| birth_place = $birth_place
| death_date  = $death_date
| death_place = $death_place
| other_names = 
| known_for   =
| occupation  = 
| nationality =
}}" ;
    $infobox_person_text = str_replace ( "\n" , "<br/>" , "$infobox_person_text\n\n" ) ;
	}
	
	# Output
	print "<h3><a target='_blank' href='{$url}'>{$name}</a> [<a target='_blank' href='{$url}&action=edit'>edit</a>]</h3>" ;
	print "<p  style='background-color:#AAAAAA'><small>" . str_replace ( '<' , '&gt;' , $ip ) . "</small></p>" ;
	print "<p style='background-color:#AAFFAA'><tt>" ;
	
	if ( $additional != '' ) print $additional . "<br/>" ;
	
	print $infobox_person_text ;
	print "{{" . $persondata . "<br/>" ;
	$out = str_replace ( "\n" , "<br>" , $persondata_text[$language] ) ;
	$out = str_replace ( '$1' , $inverted_name , $out ) ;
	$out = str_replace ( '$2' , '' , $out ) ;
	$out = str_replace ( '$3' , $desc , $out ) ;
	$out = str_replace ( '$4' , $birth_date , $out ) ;
	$out = str_replace ( '$5' , $birth_place , $out ) ;
	$out = str_replace ( '$6' , $death_date , $out ) ;
	$out = str_replace ( '$7' , $death_place , $out ) ;
	print $out . "<br/>}}</tt></p>" ;
	myflush();
}


# The end
print "</body></html>" ;

?>